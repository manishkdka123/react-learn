import React, { useEffect, useState } from 'react'
// In useEffect function,  first of all, returned part is executed except
//  first rendering of a page, then in all other renderings, returned part is executed, if the 
// useEffect function executes.
const LearnUseEffect = () => {
    let [count,setCount]=useState(0)
    useEffect(()=>{console.log("I am asynchronous useeffect")
                    return ()=>{console.log("I am return")}
        },) 
        console.log("I am first")
  return (
    <div>
        Learn useEffect2
        <br></br>
        <p>{count}</p>
        <br></br>
      <button onClick={()=>{setCount(count+1)}}>click me</button>
    </div>
  );
};

export default LearnUseEffect
