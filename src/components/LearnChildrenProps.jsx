import React from 'react'

const LearnChildrenProps = ({name,age,children}) => {
  return (
    <div>
      <p>LearnChildrenProps</p>
        {name}
        <br></br>
        {age}
        <br> </br>
        {children}
    </div>
  )
}

export default LearnChildrenProps
