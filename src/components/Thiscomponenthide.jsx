import React, { useEffect, useState } from 'react'

const Thiscomponenthide = () => {
  let [a,seta]=useState(0);
  useEffect(()=>{console.log("I am asynchronous function")
                 return ()=>{console.log("I am return")}
})
  return (
    <div style={{border:'5px solid red'}}>
       <p>This is a component that will be hidden</p>
       <img src={"../logo512.png"} style={{height:'150px',width:'150px'}}></img>
    </div>
  )
}

export default Thiscomponenthide
