import React, { useState } from 'react'

const Counter = () => {
    let [manish,khadka]=useState(0)

  return (
    <>
      {manish}
      <br></br>
      <button onClick={()=>khadka(manish+1)}>Increment</button>
      <br></br>
      <button onClick={()=>khadka(manish-1)}>Decrement</button>
      <br></br>
      <button onClick={()=>khadka(0)}>Reset</button>
    </>
  )
}

export default Counter
