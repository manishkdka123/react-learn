import React, { useState } from 'react'

const Form2 = () => {
  let countries = [
    { label: "Select Country", value: "", disabled: true },
    { label: "Nepal", value: "nepal" },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
    { label: "America", value: "america" },
    {label:"Bangladesh", value:"bang"},
  ];
  let genders=[
    {label:"Male",value:"male"},
    {label:"Female",value:"female"},
    {label:"other",value:"other"},
  ]
  let favouriteMovies=[
    {label:"Select Movie", value:"",disabled:true},
    {label:"Prison Break", value:"prison break",disabled:true},
    {label:"Breaking Bad", value:"breakingbad",disabled:true},
    {label:"Chhaka panja", value:"6ka5ja",disabled:true},
    {label:"Farzi", value:"farzi",disabled:true},
  ];
    let [name,setName]=useState("")
    let [address,setAddress]=useState("")
    let [password,setPassword]=useState("")
    let [description,setDescription]=useState("")
    let [isMarried,setIsMarried]=useState("")
    let [country,setCountry]=useState("")
    let [movie,setMovie]=useState("")
    let [gender,setGender]=useState("")
 
  return <form onSubmit={(e)=>{
    e.preventDefault();
    let info={name:name,
            address:address,
            password:password,
            description:description,
            Married: isMarried,
            country:country,
            Favourite_Movie:movie,
    };
    console.log(info)
  }}>
          
            <label htmlFor="name">Name:</label>
        <input type="text" id='name' value={name} onChange={(e)=>{setName(e.target.value);}}></input>
        <br></br>
     
        <label htmlFor="address" >Address</label>
        <input type="text"
         id='address'
          value={address} 
          onChange={(e)=>{setAddress(e.target.value)}}></input>
        <br></br>
        <label htmlFor="pass">Password</label>
        <input type="password" id="pass" value={password} onChange={(e)=>{setPassword(e.target.value);}}></input>
        <br></br>
        <label htmlFor="desc">Description:</label>
        <textarea placeholder="description" id="desc" value={description} onChange={(e)=>{setDescription(e.target.value);}}></textarea>
        <br></br>
        <label htmlFor='isMarried'>IsMarried</label>
        <input type="checkbox" id="isMarried" checked={isMarried} onChange={(e)=>{setIsMarried(e.target.checked)}}></input>
        <br></br>

        {/* THIS IS LONG AND ANCIENT METHOD  */}
        {/* <label id="country">Country</label>
        <select htmlFor="country" value="" onChange={(e)=>{setCountry(e.target.value)}}>
          <option value="" disabled={true }>Select Country</option>
          <option value="nep">Nepal</option>
          <option value="chin">China</option>
          <option value="ind">India</option>
          <option value="bang">Bangladesh</option>
        </select> */}
        <label htmlFor='country'>Country:- </label>
        <select id="country" value={country} onChange={(e)=>{setCountry(e.target.value)}}>
        {
          countries.map((obj,i)=><option key={i} value={obj.value} disabled={obj.disabled}>{obj.label}</option>)
        }
        </select>
        <br></br>
        <label htmlFor="moviee">Fovourite Movie:-</label>
        <select id="moviee" value={movie} onChange={(e)=>{setMovie(e.target.value)}}>
          {
            favouriteMovies.map((obj,index)=><option value={obj.value} disabled={obj.index} key={index}>{obj.label}</option>)
          }
          </select>
          <br></br>
          {/* <label htmlFor="gender">Gender:-</label>
          <label htmlFor='male'>Male</label>
          <input type='radio' id='male' value="male" checked={gender==='male'} onChange={(e)=>{setGender(e.target.value)}}></input>
          <label htmlFor='female'>Female</label>
          <input type='radio' id='female' value="female" checked={gender==='female'} onChange={(e)=>{setGender(e.target.value)}}></input>
          <label htmlFor='other'>Other</label>
          <input type='radio' id='other' value="other" checked={gender==='other'} onChange={(e)=>{setGender(e.target.value)}}></input> */}
          {/* <br></br>
           <label htmlFor='male'>Gender</label>
           {
            gender.map((Item,i)=>{
              return(
                <div key={i}>
                  <label htmlFor={item.value}>{item.label}</label>
                  <input
                  checked={gender===item.value}
                  onChange={(e)=>{
                    setGender(e.target.value);
                  }}}
                  ></input>
                </div>
              )
            }
           }  */}



        <button type="submit">send</button>
       </form>
}

export default Form2
