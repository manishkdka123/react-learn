import React,{useState} from 'react'

const HomeworkFormone = () => {
    let [genderstatewala,setGender]=useState('')
    let [namestatewala,setName]=useState('')
    let [agestatewala,setAge]=useState("")
    let [emailstatewala,setEmail]=useState("")
    let [productname,setProductname]=useState("")
    let [productmanageremail,setProductmanageremail]=useState("")
    let [staffemail,setStaffemail]=useState("")
    let [password,setPassword]=useState("")
    let [gender2,setGender2]=useState("")
    let [productmanufacturedate,setProductmanufacturedate]=useState("")
    let [ismarried,setIsmarried]=useState("")
    let [managerspouse,setManagerspouse]=useState("")
    let [productlocation,setProductlocation]=useState("")
    let [productdescription,setProductdescription]=useState("")
    let [isavailable,setIsavailable]=useState("")
  let spousenums=[
    {label:'One',value:1},
    {label:'Two',value:2},
    {label:'Three',value:3},
    {label:'Four',value:4},
    {label:'Five',value:5},
  ]
    let genders2=[
      {label:"Male",value:"male"},
      {label:"Female",value:"female"},
      {label:"other",value:"other"},
    ]
  return (
    <div>
      <h1>I am Assignment one</h1>
      <form>
    <label htmlFor="00">Name</label>
    <input type='text' id="00"  value={namestatewala} onChange={e=>setName(e.target.value)}></input>
    <br></br>
    <label htmlFor="01" >Age</label>
    <input type='text' id="01" value={agestatewala} onChange={e=>setAge(e.target.value)}></input>
    <br></br>
    <label htmlFor="02" >Email</label>
    <input type='text' id="02" value={emailstatewala} onChange={e=>setEmail(e.target.value)}></input>
    <br></br>
    <label>Gender:-</label>
    <label htmlFor='gen1'>Male</label>
    <input type='radio' id='gen1' value='male' checked={genderstatewala==='male'} onChange={(e)=>setGender(e.target.value)}></input>
    <label htmlFor='gen2'>Female</label>
    <input type='radio' id='gen2' value='female' checked={genderstatewala==='female'} onChange={e=>setGender(e.target.value)}></input>
    <label htmlFor='gen3'>Other</label>
    <input type='radio' id='gen3' value='other' checked={genderstatewala==='other'} onChange={e=>setGender(e.target.value)}></input>
    <br></br>
    <button type='submit'>send</button>
    </form>
    <h1>I am assignment two</h1>
    <form>
      <label htmlFor='10'>Product Name:-</label>
      <input type='text' id='10' value={productname} onChange={(e)=>{setProductname(e.target.value)}}></input>
      <br></br>
      <label htmlFor='11'>Product Manager Email:-</label>
      <input type='text' id='11' value={productmanageremail} onChange={(e)=>{setProductmanageremail(e.target.value)}}></input>
      <br></br>
      <label htmlFor='12'>Staff Email:-</label>
      <input type='email' id='12' value={staffemail} onChange={(e)=>{setStaffemail(e.target.value)}}></input>
      <br></br>
      <label htmlFor='13'>Password:-</label>
      <input type='text' id='13' value={password} onChange={(e)=>{setPassword(e.target.value)}}></input>
      <br></br>
      <label htmlFor='14'>Gender:-</label>
      {
      genders2.map((obj,index)=>{
      return <div key={index}> 
      <label htmlFor={obj.value}>{obj.label}</label>
      <input type='radio' id={obj.value} value={obj.value} checked={gender2===obj.value} onChange={(e)=>{setGender2(e.target.value)}}></input>
              </div>
      })
    }
    <label htmlFor="15">Product Manufacture:-</label>
    <input type='text' id='15' value={productmanufacturedate} onChange={(e)=>{setProductmanufacturedate(e.target.vale)}}></input>
    <br></br>
    <label htmlFor='16'>isMarried:- </label>
    <label htmlFor='yess'>Yes</label>
    <input type='radio' id='yess' value='yes' checked={ismarried==='yes'} onChange={(e)=>{setIsmarried(e.target.value)}}></input>
    <label htmlFor='noo'>No</label>
    <input type='radio' id='noo' value='no' checked={ismarried==='no'} onChange={(e)=>{setIsmarried(e.target.value)}}></input>
      <br></br>
    <label>Manager Spouse Number:-</label>
    {
      spousenums.map((obj,index)=>{
        return <div key={index}>
          <label htmlFor={obj.value}>{obj.label}</label>
          <input id={obj.value} type='checkbox' checked={obj.value}  onChange={e=>setManagerspouse(e.target.checked)}></input>
        </div>
      })
    }
    <br></br>
    <label htmlFor='17'>Product Location:-</label>
    <input type='text' id='17' value={productlocation} onChange={e=>setProductlocation(e.target.value)}></input>
    <br></br>
    <label htmlFor='18'>Product Description:-</label>
    <input type='text' id='18' value={productdescription} onChange={e=>setProductdescription(e.target.value)}></input>
    <br></br>
    <label htmlFor='19'>Is the product Available:-</label>
    <label htmlFor='190'>Yes</label>
    <input type='checkbox' id='190' checked={isavailable} onChange={(e)=>setIsavailable(e.target.checked)}></input>
    <label htmlFor='191'>NO</label>
    <input type='checkbox' id='191' checked={isavailable} onChange={(e)=>setIsavailable(e.target.checked)}></input>
    
    </form>
   
    </div>
  )
}

export default HomeworkFormone
