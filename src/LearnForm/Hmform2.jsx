import React, { useState } from 'react'

const Hmform2 = () => {
    let [email,setEmail]=useState("")
    let [phno,setPhno] = useState("")
    let [pass,setPass]=useState("")
    let [ismar,setIsmar]=useState("")
    let [clothsize,setClothsize]=useState("")
    let [genderstatewala,setGender]=useState("")
    let [comment,setComment]=useState("")
    let [role,setRole]=useState({admin:false,superadmin:false,customer:false,deliveryperson:false})

    const Roles=[
        {label: 'admin',value:'admin'},
        {label: 'superadmin',value:'superadmin'},
        {label: 'customer',value:'customer'},
        {label:'deliveryperson',value:'deliveryperson'},
    ]

  return (
    <div>
      <h1>I am assignment three</h1>
      <form onSubmit={(e)=>{
        e.preventDefault()
        let obj = {
          role
        }

        console.log(obj)
      }}>
      <label htmlFor='000'>Email:-</label>
      <input type='email' id='000' value={email} onChange={(e)=>{setEmail(e.target.value)}}></input>
      <br></br>
      <label htmlFor='001'>Phone Number:-</label>
        <input type='number' id='001' value={phno} onChange={e=>setPhno(e.value.target)}></input>
        <br></br>
        <label htmlFor='002'>Password:-</label>
        <input type='password' id='002' value={pass} onChange={(e)=>setPass(e.target.value)}></input>
            <br></br>
        <label htmlFor='003'>Is Married?</label>
        <label htmlFor="yes">Yes</label>
        <input type='checkbox' id='yes' checked={ismar} onChange={e=>setIsmar(e.target.checked)}></input>
        <label htmlFor='no'>No</label>
        <input type='checkbox' id='no' checked={ismar} onChange={e=>setIsmar(e.target.checked)}></input>
        <br></br>
        <label htmlFor='004'>Cloth Size:- </label>
        <input type='text' id='004' value={clothsize} onChange={e=>setClothsize(e.target.checked)}></input>
        <br></br>
        <label>Gender:-</label>
    <label htmlFor='gen11'>Male</label>
    <input type='radio' id='gen11' value='male' checked={genderstatewala==='male'} onChange={(e)=>setGender(e.target.value)}></input>
    <label htmlFor='gen22'>Female</label>
    <input type='radio' id='gen22' value='female' checked={genderstatewala==='female'} onChange={e=>setGender(e.target.value)}></input>
    <label htmlFor='gen33'>Other</label>
    <input type='radio' id='gen33' value='other' checked={genderstatewala==='other'} onChange={e=>setGender(e.target.value)}></input>
    <br></br>
    <label htmlFor='cmt'>Comment:-</label>
    <input type='textarea' id='cmt' value={comment} onChange={e=>setComment(e.target.value)}></input>
    <br></br>
    <label htmlFor='role'>Role:-</label>
    {
        Roles.map((obj,index)=>{
            return <div key={index}>
                <label htmlFor={obj.label}>{obj.label}</label>
                <input type='checkbox' id={obj.label} checked={role[obj.label]} onChange={e=>setRole({...role,[obj.label]:e.target.value})}></input>
            </div> 
        })
    }
    <br></br>

    <input type="submit" value="submit" />
      </form>
    </div>
  )
}

export default Hmform2
