import React, { useState } from 'react'
const Classform = () => {
  let [name,setName] = useState("")
  let [address,setAddress] = useState("")
  let [phone,setPhone] = useState("")
  let [email,setEmail] = useState("")
  return (
    <form
    onSubmit={(e) => {
        e.preventDefault()
        let info = [
            name,
            address,
            phone,
            email,
        ]
        console.log(info)
    }}
    >
    <label htmlFor='name'>Name: </label>
    <input id='name'
            type='text'
            placeholder='Enter your name'
            value={name}
            onChange={(e) => {
                setName(e.target.value);
            }}
    ></input>
<br></br>
<label htmlFor='address'>address: </label>
    <input id='address'
            type='text'
            placeholder='Enter your address'
            value={address}
            onChange={(e) => {
                setAddress(e.target.value);
            }}
    ></input>
<br></br>
<label htmlFor='phone'>phone number: </label>
    <input id='phone'
            type='number'
            placeholder='Enter your phone number'
            value={phone}
            onChange={(e) => {
                setPhone(e.target.value);
            }}
    ></input>
<br></br>
<label htmlFor='email'>email: </label>
    <input id='email'
            type='email'
            placeholder='Enter your email'
            value={email}
            onChange={(e) => {
                setEmail(e.target.value);
            }}
    ></input>
    </form>
    )
}
export default Classform