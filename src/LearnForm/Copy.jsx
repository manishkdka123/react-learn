import React, { useState } from 'react'
import axios from 'axios'

const Copy = () => {
    let [firstName,setFirstName]=useState("")
    let [address,setAddress]=useState("")
    let [email,setEmail]=useState("")
    let [phone,setPhone]=useState("")
    let hitApi= async ()=>{
        let info={
            url:"https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method:"get",
        }
        return await axios(info)
       
    }
  return (
    <div>
      <form 
      onSubmit={(e) => {
        e.preventDefault();
        
        let info={
            firstName,address,email,phone,
        }
        console.log(info)
        let result=hitApi()
        console.log(result.message)
        console.log(result)
      }}
      >
        <label htmlFor="one">First Name:-</label>
        <input type='text' id="one" value={firstName} onChange={(e)=>{setFirstName(e.target.value)}}></input> 
        <br></br>
        <label htmlFor="two">Address:-</label>
        <input type='text' id="two" value={address} onChange={(e)=>{setAddress(e.target.value)}}></input>
        <br></br>
        <label htmlFor="three">Email:</label>
        <input type='email' id="three" value={email} onChange={(e)=>{setEmail(e.target.value)}}></input>
        <br></br>
        <label htmlFor="four">Phone number</label>
        <input type='number' id="four" value={phone} onChange={(e)=>{setPhone(e.target.value)}}></input>
        <br></br>
        <button type='submit'>Send</button>

      </form>
    </div>
  )
}

export default Copy
