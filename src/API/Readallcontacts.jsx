import React, { useEffect, useState } from 'react'
import { baseUrl } from '../config/config';
import axios from 'axios';

const Readallcontacts = () => {
    let [contacts,setContacts]=useState([]);
    let deleteContact=async (_id)=>{
        let info={
        url:`${baseUrl}/contacts/${_id}`,
        method:"delete",
        }
        await axios(info);
    }
    let readAllContact=async ()=>{
        let info={
            url: `${baseUrl}/contacts`,
            method:"get",
        }
        let result=await axios(info);
        setContacts(result.data.data.results)
    }
    useEffect(()=>{
        readAllContact()
    },[]);

    console.log(contacts)
  return (
    <div>
        <p>Read all contact</p>
        {
            contacts.map((obj,index)=>{
                return(
                    <div style={{border:"solid red 3px"}} key={index}>
                        <p>Full name:{obj.fullName}</p>
                        <p>Address:{obj.address}</p>
                        <p>Email:{obj.email}</p>
                        <p>Phone Number:{obj.phoneNumber}</p>
                        <button onClick={async ()=>{
                            await deleteContact(obj._id)
                            await readAllContact()
                        }
                        }>delete</button>
                        </div>
                )
            })
        }
    </div>
  )
}

export default Readallcontacts
