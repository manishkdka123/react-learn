import React, { useState } from 'react'


const LearnUseState = () => {
    let [count1,setCount1]=useState(0);
    function incremental(){count1=count1+Math.random()*10; return count1}
  return (
    <div>
      {count1}
      <br></br>
      <button onClick={()=>{setCount1(incremental())}}>Click me</button>
    
    </div>
  )
}

export default LearnUseState
